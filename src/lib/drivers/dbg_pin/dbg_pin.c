/**
 *****************************************************************************
 * @title   timers.c
 * @author  Daniel Schnell <dschnell@posteo.de>
 * @brief
 *****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

#include "stm32f0xx.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"

#include "FreeRTOS.h"

#include "string.h"
#include "dbg_pin.h"

/* definitions */


/* typedefs */


/* global variables */


/* Forward function declarations */


/* Function definitions */


/**
 * Initializes debug pin facility.
 */
void dbg_pin_init()
{
    /* PB0, PB1 Config for scope debugging */
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    dbg_pin(0x3, false);
    dbg_pin(0x3, true);
    dbg_pin(0x3, false);
}



/**
 * Switches on/off pins according to given pin mask and parameter on.
 *
 * @param pin_mask      bit mask of pins where each bit corresponds to
 *                      a GPIO output pin
 * @param on            if true: switch on pins according to given pn_mask
 *                      if false: switch pins off
 */
void dbg_pin(uint8_t pin_mask, bool on)
{
    if (pin_mask & 0x1)
    {
        if (on)
            GPIO_WriteBit(GPIOC, GPIO_Pin_4, Bit_SET);
        else
            GPIO_WriteBit(GPIOC, GPIO_Pin_4, Bit_RESET);
    }

    if (pin_mask & 0x2)
    {
        if (on)
            GPIO_WriteBit(GPIOC, GPIO_Pin_5, Bit_SET);
        else
            GPIO_WriteBit(GPIOC, GPIO_Pin_5, Bit_RESET);
    }
}

/* EOF */
